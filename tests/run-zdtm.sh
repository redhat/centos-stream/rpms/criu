#!/bin/bash

set -x

uname -a

# These zdtm tests are skipped because most of them rely
# on the iptables binary.
EXCLUDES=" \
	-x zdtm/static/net_lock_socket_iptables \
	-x zdtm/static/net_lock_socket_iptables6 \
	-x zdtm/static/netns-nf \
	-x zdtm/static/netns_lock_iptables \
	-x zdtm/static/socket-tcp-closed-last-ack \
	-x zdtm/static/socket-tcp-nfconntrack \
	-x zdtm/static/socket-tcp-reseted \
	-x zdtm/static/socket-tcp-syn-sent \
	-x zdtm/static/mntns_link_remap \
	-x zdtm/static/unlink_fstat03 \
	-x zdtm/static/unlink_regular00 "

run_test() {
	./zdtm.py run --criu-bin /usr/sbin/criu ${EXCLUDES} \
		-a --ignore-taint --keep-going

	RESULT=$?
}


RESULT=42

# this socket breaks CRIU's test cases
rm -f /var/lib/sss/pipes/nss

cd source

echo "Build CRIU"
make

which criu
rpm -qf `which criu`

cd test

echo "Run the actual CRIU test suite"
run_test

if [ "$RESULT" -ne "0" ]; then
	# Run tests a second time to make sure it is a real failure
	echo "Something failed. Run the actual CRIU test suite a second time"
	run_test
	if [ "$RESULT" -ne "0" ]; then
		echo "Still a test suite error. Something seems to be actually broken"
		exit $RESULT
	fi
fi

exit 0
